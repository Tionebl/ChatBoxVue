import { createRouter, createWebHistory } from 'vue-router'
import ChatBox from '@/views/chatbox.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: ChatBox
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router

